package com.intentsoftware.addapptr.example;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.BannerPlacementLayout;
import com.intentsoftware.addapptr.PlacementSize;

public class ExampleApplication extends Application implements AATKit.Delegate {

    private int smallBannerPlacementId = -1;
    private int wideBannerPlacementId = -1;
    private int fullscreenPlacementId = -1;
    private int videoPlacementId = -1;
    private RewardedVideoListener listener;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        AATKit.init(this, this);

        smallBannerPlacementId = AATKit.createPlacement("Banner", PlacementSize.Banner320x53);
        wideBannerPlacementId = AATKit.createPlacement("Wide_banner", PlacementSize.Banner768x90);
        fullscreenPlacementId = AATKit.createPlacement("Fullscreen", PlacementSize.Fullscreen);
        videoPlacementId = AATKit.createPlacement("RewardedVideoInterstitial", PlacementSize.Fullscreen);
        AATKit.preparePromo();
    }

    public int getSmallBannerPlacementId() {
        return smallBannerPlacementId;
    }


    public int getWideBannerPlacementId() {
        return wideBannerPlacementId;
    }

    public int getFullscreenPlacementId() {
        return fullscreenPlacementId;
    }

    public int getVideoPlacementId() {
        return videoPlacementId;
    }

    @Override
    public void aatkitHaveAd(int placementId) {
        if (placementId == videoPlacementId && listener != null) {
            listener.onLoadedVideo();
        }
    }

    @Override
    public void aatkitNoAd(int arg0) {

    }

    @Override
    public void aatkitPauseForAd(int arg0) {

    }

    @Override
    public void aatkitResumeAfterAd(int arg0) {

    }

    @Override
    public void aatkitShowingEmpty(int arg0) {
    }

    @Override
    public void aatkitObtainedAdRules(boolean arg0) {
    }

    @Override
    public void aatkitUnknownBundleId() {
    }

    @Override
    public void aatkitHaveAdForPlacementWithBannerView(int i, BannerPlacementLayout bannerPlacementLayout) {

    }

    @Override
    public void aatkitUserEarnedIncentive(int placementId) {
        if (placementId == videoPlacementId && listener != null) {
            listener.onEarnedIncentive();
        }
    }

    public void setRewardedVideoListener(RewardedVideoListener listener) {
        this.listener = listener;
    }

}
