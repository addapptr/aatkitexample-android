package com.intentsoftware.addapptr.example;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.BannerSize;

public class MainActivity extends Activity implements RewardedVideoListener {

	private int usedBannerPlacementId = -1;
	private int smallBannerPlacmentId = -1;
	private int wideBannerPlacmentId = -1;
	private int fullscreenPlacementId = -1;
	private int videoPlacementId = -1;
	private static final int testAppId = 136;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		AATKit.enableTestMode(testAppId);

		smallBannerPlacmentId = ((ExampleApplication) getApplication()).getSmallBannerPlacementId();
		wideBannerPlacmentId = ((ExampleApplication) getApplication()).getWideBannerPlacementId();
		fullscreenPlacementId = ((ExampleApplication) getApplication()).getFullscreenPlacementId();
		videoPlacementId = ((ExampleApplication) getApplication()).getVideoPlacementId();
		((ExampleApplication) getApplication()).setRewardedVideoListener(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) != ConnectionResult.SUCCESS) {
			Dialog d = GoogleApiAvailability.getInstance().getErrorDialog(this,
					GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this), 4);
			d.show();
		}

		AATKit.onActivityResume(this);

		DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
		float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

		if (dpWidth >= BannerSize.Banner768x90.getWidth()) {
			usedBannerPlacementId = wideBannerPlacmentId;
		} else {
			usedBannerPlacementId = smallBannerPlacmentId;
		}

		addPlacementView(usedBannerPlacementId);
		AATKit.startPlacementAutoReload(usedBannerPlacementId);
		AATKit.startPlacementAutoReload(fullscreenPlacementId);
		AATKit.startPlacementAutoReload(videoPlacementId);
	}

	@Override
	protected void onPause() {

		AATKit.stopPlacementAutoReload(usedBannerPlacementId);
		AATKit.stopPlacementAutoReload(fullscreenPlacementId);
		AATKit.stopPlacementAutoReload(videoPlacementId);

		removePlacementView(usedBannerPlacementId);

		AATKit.onActivityPause(this);

		super.onPause();
	}

	private void addPlacementView(int placementId) {
		RelativeLayout mainLayout = (RelativeLayout) findViewById(R.id.main_layout);
		View placementView = AATKit.getPlacementView(placementId);
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		mainLayout.addView(placementView, layoutParams);
	}

	private void removePlacementView(int placementId) {
		View placementView = AATKit.getPlacementView(placementId);
		if (placementView.getParent() != null) {
			ViewGroup parent = (ViewGroup) placementView.getParent();
			parent.removeView(placementView);
		}
	}

	public void onShowFullscreenButtonClick(View button) {
		AATKit.showPlacement(fullscreenPlacementId);
	}

	public void onShowRVButtonClick(View button) {
		button.setEnabled(false);
		AATKit.showPlacement(videoPlacementId);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_about:
			onAboutButtonClick(null);
			return true;
		case R.id.menu_show_promo:
			showPromo();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

    private void showPromo() {
        AATKit.showPromo(false);
    }

	public void onAboutButtonClick(View view) {
		String message = "AATKit version:\n" + AATKit.getFullVersion();

		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		dialogBuilder.setTitle(R.string.app_name);
		dialogBuilder.setMessage(message);
		dialogBuilder.setPositiveButton(R.string.ok, null);
		AlertDialog dialog = dialogBuilder.create();
		dialog.show();
	}

	@Override
	public void onLoadedVideo() {
		View showVideoButton = findViewById(R.id.showRVButton);
		showVideoButton.setEnabled(true);
	}

	@Override
	public void onEarnedIncentive() {
		Toast.makeText(this, "Incentive earned", Toast.LENGTH_LONG).show();
	}
}
