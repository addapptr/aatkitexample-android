package com.intentsoftware.addapptr.example;

public interface RewardedVideoListener {
	
	public void onLoadedVideo();
	public void onEarnedIncentive();
}
